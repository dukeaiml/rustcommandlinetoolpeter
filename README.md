# Rust Command Line Tool to Get the Weather in Any City.


## Table of Contents
1. [Deliverables](#deliverables)
2. [Description](#description)
3. [Installation and Usage](#Installation and Usage)
4. [Development Process](#development-process)

## Deliverables
Sample Output: 

![Sample output](Screenshot_2024-04-04_at_7.05.47_PM.png)

Testing report:

![Testing report](Screenshot_2024-04-04_at_7.05.57_PM.png)



## Description

This is a Rust CLI which allows the user to input a city name, which calls a home.openweathermap API, which then returns the current temperature, wind, visibility, and humidity at that location.

It is complete with built in Rust testing to make sure the information is parsed correctly, the API is called correctly, and that the information is reasonable.

Check src/main.rs to see the source code.

## Installation and Usage

To run this project locally, ensure Rust, Cargo, are installed on your system. Follow these installation steps:

1. Clone the repository:


git clone git@gitlab.com:dukeaiml/rustcommandlinetoolpeter.git

2. Invoke the function with

cargo run --"Paris"

Test it with 

cargo test.


## Development Process

Development steps included:

1. Environment Setup: Install the necessary dependencies for rust.
2. Implementation: Write structs for the weather. Set up CLI.
3. Implementation: Take and parse API returns and handle inputs/outputs.
3. Testing: Ensuring functionality and handling errors. Used [cfg(test)]
4. Documentation: Documenting code and writing this README.


