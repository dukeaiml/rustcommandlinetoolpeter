use clap::{App, Arg};
use reqwest::blocking;
use serde::Deserialize;
use std::{error::Error, fmt};

#[derive(Debug, Deserialize)]
struct WeatherReport {
    main: WeatherMain,
    weather: Vec<WeatherCondition>,
    wind: Wind,
    visibility: u32,
}

#[derive(Debug, Deserialize)]
struct WeatherMain {
    temp: f64,
    humidity: u8,
}

#[derive(Debug, Deserialize)]
struct WeatherCondition {
    main: String,
    description: String,
}

#[derive(Debug, Deserialize)]
struct Wind {
    speed: f64,
    deg: u16,
}

#[derive(Debug)]
enum WeatherError {
    Reqwest(reqwest::Error),
    Serde(serde_json::Error),
}

impl fmt::Display for WeatherError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            WeatherError::Reqwest(e) => write!(f, "Request error: {}", e),
            WeatherError::Serde(e) => write!(f, "Parsing error: {}", e),
        }
    }
}

impl Error for WeatherError {}

impl From<reqwest::Error> for WeatherError {
    fn from(error: reqwest::Error) -> Self {
        WeatherError::Reqwest(error)
    }
}

impl From<serde_json::Error> for WeatherError {
    fn from(error: serde_json::Error) -> Self {
        WeatherError::Serde(error)
    }
}

fn fetch_weather(location: &str) -> Result<WeatherReport, WeatherError> {
    let api_key = "7c534fd4c2f5d79273de97d13e91f4d1"; // Replace with your actual API key
    let request_url = format!(
        "https://api.openweathermap.org/data/2.5/weather?q={}&appid={}&units=metric",
        location, api_key
    );

    let response_body = reqwest::blocking::get(request_url)?.text()?;
    let response = serde_json::from_str::<WeatherReport>(&response_body)?;
    Ok(response)
}

fn main() {
    let matches = App::new("WeatherStation")
        .version("1.0")
        .author("Your Name")
        .about("Fetches weather information")
        .arg(Arg::with_name("LOCATION")
            .help("Sets the location to fetch weather for")
            .required(true)
            .index(1))
        .get_matches();

    let location = matches.value_of("LOCATION").unwrap();

    match fetch_weather(location) {
        Ok(report) => {
            println!("Current weather in {}: {}°C, {}",
                location,
                report.main.temp,
                report.weather.first().map_or("", |w| &w.description)
            );
            println!("Wind: {} m/s at {} degrees", report.wind.speed, report.wind.deg);
            println!("Visibility: {} meters", report.visibility);
            println!("Humidity: {}%", report.main.humidity);
        },
        Err(e) => println!("Error fetching weather data: {}", e),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // This test assumes you have an example JSON string that closely resembles an actual API response.
    // The key here is not to verify the specific weather data but to ensure that your application
    // can correctly parse and handle the data structure of a typical response.
    #[test]
    fn test_weather_report_parsing_structural() {
        let json_data = r#"
        {
            "main": {"temp": 20.0, "humidity": 50},
            "weather": [{"main": "Clouds", "description": "overcast clouds"}],
            "wind": {"speed": 5.5, "deg": 220},
            "visibility": 10000
        }
        "#;

        // Test successful deserialization
        let weather: Result<WeatherReport, _> = serde_json::from_str(json_data);
        assert!(weather.is_ok());

        let weather = weather.expect("Failed to parse weather data.");

        // Structural checks (Adjust according to your actual data model expectations)
        assert!(weather.main.temp > -90.0 && weather.main.temp < 60.0, "Temperature is out of expected range.");
        assert!(weather.main.humidity <= 100, "Humidity is out of expected range.");
        assert!(!weather.weather.is_empty(), "Weather conditions should not be empty.");
        assert!(weather.wind.speed >= 0.0, "Wind speed should be a non-negative value.");
    }

    // Additional structural or functional tests can go here
}
